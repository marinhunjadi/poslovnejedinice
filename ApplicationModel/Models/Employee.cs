﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationModel.Models
{
    public class Employee
    {
        public int EmployeeID { get; set; }        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OIB { get; set; }
        public bool IsUnitLeader { get; set; }
        public string OfficialVehicleName { get; set; }

        public int UnitID { get; set; }
        public Unit Unit { get; set; }

        public virtual List<Skill> Skills { get; set; }
    }
}
