﻿using ApplicationData.Infrastrucure;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationData.Repositories
{
    public class UnitRepository : RepositoryBase<Unit>, IUnitRepository
    {
        public UnitRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        /*public Unit GetCityByName(string cityName)
        {
            var category = this.DbContext.Cities.Where(c => c.Name == cityName).FirstOrDefault();

            return category;
        }*/

        /*public override void Update(City entity)
        {
            entity.DateUpdated = DateTime.Now;
            base.Update(entity);
        }*/
    }

    public interface IUnitRepository : IRepository<Unit>
    {
        //Unit GetCityByName(string cityName);
    }
}
