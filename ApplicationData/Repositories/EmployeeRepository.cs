﻿using ApplicationData.Infrastrucure;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationData.Repositories
{
    public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        /*public IEnumerable<Employee> GetEmployeesByUnitId(int unitID, int start, int length)
        {
            var employeess = this.DbContext.;

            if(cityID > 0)
                return restaurants.Where(r => r.UnitID == cityID).OrderBy(r => r.Name).Skip(start).Take(length).ToList();
            else
                return restaurants.OrderBy(r => r.Name).Skip(start).Take(length).ToList();
        }*/
    }

    public interface IEmployeeRepository : IRepository<Employee>
    {
        //IEnumerable<Employee> GetEmployeesByUnitId(int unitID, int start, int length);
    }
}
