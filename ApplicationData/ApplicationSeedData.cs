﻿using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationData
{
    public class ApplicationSeedData : DropCreateDatabaseIfModelChanges<ApplicationEntities>
    {
        protected override void Seed(ApplicationEntities context)
        {
            GetUnits().ForEach(u => context.Units.Add(u));
            context.Commit();
            GetEmployees().ForEach(e => context.Employees.Add(e));
            context.Commit();
            GetSkills().ForEach(s => context.Skills.Add(s));
            context.Commit();            
        }

        private static List<Unit> GetUnits()
        {
            return new List<Unit>
            {
                new Unit {
                    Name = "Zagreb",
                    Level = 1
                },
                new Unit {
                    Name = "Split",
                    Level = 2,
                    ParentID = 1
                },
                new Unit {
                    Name = "Rijeka",
                    Level = 2,
                    ParentID = 1
                },
                new Unit {
                    Name = "Pula",
                    Level = 1
                },
                new Unit {
                    Name = "Umag",
                    Level = 2,
                    ParentID = 4
                }
            };
        }

        private static List<Employee> GetEmployees()
        {
            return new List<Employee>
            {
                new Employee {
                    FirstName = "A",
                    LastName = "B",
                    OIB = "12345678901",
                    IsUnitLeader = true,
                    OfficialVehicleName = "Yugo 45",
                    UnitID = 1
                },
                new Employee {
                    FirstName = "C",
                    LastName = "D",
                    OIB = "12345678901",
                    IsUnitLeader = false,
                    UnitID = 1
                },
                new Employee {
                    FirstName = "E",
                    LastName = "F",
                    OIB = "12345678901",
                    IsUnitLeader = false,
                    UnitID = 1
                },
                new Employee {
                    FirstName = "I",
                    LastName = "J",
                    OIB = "12345678901",
                    IsUnitLeader = true,
                    OfficialVehicleName = "Yugo 45",
                    UnitID = 4
                },
                new Employee {
                    FirstName = "K",
                    LastName = "L",
                    OIB = "12345678901",
                    IsUnitLeader = false,
                    UnitID = 4
                },
                new Employee {
                    FirstName = "M",
                    LastName = "N",
                    OIB = "12345678901",
                    IsUnitLeader = false,
                    UnitID = 4
                }
            };
        }

        private static List<Skill> GetSkills()
        {
            return new List<Skill>
            {
                new Skill {
                    Name = "Skill 1",
                    EmployeeID = 2
                },
                new Skill {
                    Name = "Skill 1",
                    EmployeeID = 3
                },
                new Skill {
                    Name = "Skill 1",
                    EmployeeID = 5
                },
                new Skill {
                    Name = "Skill 1",
                    EmployeeID = 6
                },
                new Skill {
                    Name = "Skill 2",
                    EmployeeID = 2
                },
                new Skill {
                    Name = "Skill 2",
                    EmployeeID = 3
                }
            };
        }
    }
}
