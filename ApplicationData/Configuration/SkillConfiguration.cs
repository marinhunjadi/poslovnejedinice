﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using ApplicationModel.Models;

namespace ApplicationData.Configuration
{
    public class SkillConfiguration : EntityTypeConfiguration<Skill>
    {
        public SkillConfiguration()
        {
            ToTable("Skills");
            Property(s => s.Name).IsRequired().HasMaxLength(255);
            Property(s => s.EmployeeID).IsRequired();
        }
    }
}
