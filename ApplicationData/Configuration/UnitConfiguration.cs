﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using ApplicationModel.Models;

namespace ApplicationData.Configuration
{
    public class UnitConfiguration : EntityTypeConfiguration<Unit>
    {
        public UnitConfiguration()
        {
            ToTable("Units");
            Property(u => u.Name).IsRequired().HasMaxLength(255);
            Property(u => u.Level).IsRequired();
        }
    }
}
