﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
//using System.Data.Spatial;
using ApplicationModel.Models;

namespace ApplicationData.Configuration
{
    public class EmployeeConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfiguration()
        {
            ToTable("Employees");
            Property(e => e.FirstName).IsRequired().HasMaxLength(100);
            Property(e => e.LastName).IsRequired().HasMaxLength(100);
            Property(e => e.OIB).IsRequired().IsFixedLength().HasMaxLength(11);
            Property(e => e.UnitID).IsRequired();
        }
    }
}
