﻿using ApplicationData.Configuration;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationData
{
    public class ApplicationEntities : DbContext
    {
        public ApplicationEntities() : base("DefaultConnection") { }

        public DbSet<Unit> Units { get; set; }
        public DbSet<Employee> Employees { get; set; }        
        public DbSet<Skill> Skills { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UnitConfiguration());
            modelBuilder.Configurations.Add(new EmployeeConfiguration());            
            modelBuilder.Configurations.Add(new SkillConfiguration());
        }
    }
}
