﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoslovneJedinice.ViewModels
{
    public class UnitViewModel
    {
        public int UnitID { get; set; }
        public int? ParentID { get; set; }
        public int Level { get; set; }
        public string Name { get; set; }

        public List<EmployeeViewModel> Employees { get; set; }
    }
}