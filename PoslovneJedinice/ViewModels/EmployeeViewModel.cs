﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoslovneJedinice.ViewModels
{
    public class EmployeeViewModel
    {
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OIB { get; set; }
        public bool IsUnitLeader { get; set; }
        public string OfficialVehicleName { get; set; }

        public int UnitID { get; set; }
    }
}