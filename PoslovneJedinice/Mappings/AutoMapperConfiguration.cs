﻿using ApplicationModel.Models;
using AutoMapper;
using PoslovneJedinice.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoslovneJedinice.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Unit, UnitViewModel>();
                cfg.CreateMap<Employee, EmployeeViewModel>();
            });
        }
    }
}