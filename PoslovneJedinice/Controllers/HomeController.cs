﻿using ApplicationModel.Models;
using ApplicationService;
using AutoMapper;
using PoslovneJedinice.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoslovneJedinice.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitService unitService;
        private readonly IEmployeeService employeeService;

        public HomeController(IUnitService unitService, IEmployeeService employeeService)
        {
            this.unitService = unitService;
            this.employeeService = employeeService;
        }

        public ActionResult Index()
        {
            //IEnumerable<UnitViewModel> viewModelUnits;
            IEnumerable<Unit> units;

            units = unitService.GetUnits().ToList();

            //viewModelUnits = Mapper.Map<IEnumerable<Unit>, IEnumerable<UnitViewModel>>(units);
            ViewBag.Level = 1;            
            return View(units);
        }

        public ActionResult About(int id)
        {
            //UnitViewModel viewModelUnit;
            Unit unit;

            unit = unitService.GetUnit(id);

            //viewModelUnit = Mapper.Map<Unit, UnitViewModel>(unit);

            IEnumerable<Employee> employees = employeeService.GetEmployees().ToList();
            IEnumerable<Unit> childUnits = unitService.GetChildUnits(id).ToList();
            ViewBag.Employees = employees;
            ViewBag.ChildUnits = childUnits;
            return View(unit);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}