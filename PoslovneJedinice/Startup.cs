﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PoslovneJedinice.Startup))]
namespace PoslovneJedinice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
