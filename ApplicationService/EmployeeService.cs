﻿using ApplicationData.Infrastrucure;
using ApplicationData.Repositories;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService
{
    // operations you want to expose
    public interface IEmployeeService
    {
        IEnumerable<Employee> GetEmployees();
        //IEnumerable<Employee> GetUnitEmployees(string unitName, string employeeName = null);
        //IEnumerable<Employee> GetEmployeesByUnitId(int unitID, int start, int length);
        Employee GetEmployee(int id);
        void CreateEmployee(Employee employee);
        void SaveEmployee();
    }

    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository employeesRepository;
        private readonly IUnitRepository unitRepository;
        private readonly IUnitOfWork unitOfWork;

        public EmployeeService(IEmployeeRepository employeesRepository, IUnitRepository unitRepository, IUnitOfWork unitOfWork)
        {
            this.employeesRepository = employeesRepository;
            this.unitRepository = unitRepository;
            this.unitOfWork = unitOfWork;
        }

        #region IEmployeeService Members

        public IEnumerable<Employee> GetEmployees()
        {
            var employees = employeesRepository.GetAll();
            return employees;
        }

        /*public IEnumerable<Employee> GetUnitEmployees(string unitName, string employeeName = null)
        {
            var unit = unitRepository.GetUnitByName(unitName);
            return unit.Employees.Where(g => g.Name.ToLower().Contains(employeeName.ToLower().Trim()));
        }*/

        /*public IEnumerable<Employee> GetEmployeesByUnitId(int unitID, int start, int length)
        {
            return employeesRepository.GetEmployeesByUnitId(unitID, start, length);
        }*/

        public Employee GetEmployee(int id)
        {
            var employee = employeesRepository.GetById(id);
            return employee;
        }

        public void CreateEmployee(Employee employee)
        {
            employeesRepository.Add(employee);
        }

        public void SaveEmployee()
        {
            unitOfWork.Commit();
        }

        #endregion
    }
}
