﻿using ApplicationData.Infrastrucure;
using ApplicationData.Repositories;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService
{
    // operations you want to expose
    public interface IUnitService
    {
        IEnumerable<Unit> GetUnits(string name = null);
        IEnumerable<Unit> GetChildUnits(int id);
        Unit GetUnit(int id);
        //Unit GetUnit(string name);
        void CreateUnit(Unit unit);
        void SaveUnit();
    }

    public class UnitService : IUnitService
    {
        private readonly IUnitRepository unitRepository;
        private readonly IUnitOfWork unitOfWork;

        public UnitService(IUnitRepository unitRepository, IUnitOfWork unitOfWork)
        {
            this.unitRepository = unitRepository;
            this.unitOfWork = unitOfWork;
        }

        #region IUnitService Members

        public IEnumerable<Unit> GetUnits(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                return unitRepository.GetAll().OrderBy(u => u.ParentID);
            else
                return unitRepository.GetAll().Where(c => c.Name == name).OrderBy(u => u.ParentID);
        }

        public IEnumerable<Unit> GetChildUnits(int id)
        {
            return unitRepository.GetAll().Where(c => c.ParentID == id);
        }

        public Unit GetUnit(int id)
        {
            var unit = unitRepository.GetById(id);
            return unit;
        }

        /*public Unit GetUnit(string name)
        {
            var unit = unitRepository.GetUnitByName(name);
            return unit;
        }*/

        public void CreateUnit(Unit unit)
        {
            unitRepository.Add(unit);
        }

        public void SaveUnit()
        {
            unitOfWork.Commit();
        }

        #endregion
    }
}
