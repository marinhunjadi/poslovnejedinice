﻿using ApplicationData.Infrastrucure;
using ApplicationData.Repositories;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService
{
    // operations you want to expose
    public interface ISkillService
    {
        IEnumerable<Skill> GetSkills(string name = null);
        Skill GetSkill(int id);
        //Skill GetSkill(string name);
        void CreateSkill(Skill skill);
        void SaveSkill();
    }

    public class SkillService : ISkillService
    {
        private readonly ISkillRepository skillRepository;
        private readonly IUnitOfWork unitOfWork;

        public SkillService(ISkillRepository skillRepository, IUnitOfWork unitOfWork)
        {
            this.skillRepository = skillRepository;
            this.unitOfWork = unitOfWork;
        }

        #region ISkillService Members

        public IEnumerable<Skill> GetSkills(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                return skillRepository.GetAll();
            else
                return skillRepository.GetAll().Where(c => c.Name == name);
        }

        public Skill GetSkill(int id)
        {
            var skill = skillRepository.GetById(id);
            return skill;
        }

        /*public Skill GetSkill(string name)
        {
            var skill = skillRepository.GetSkillByName(name);
            return skill;
        }*/

        public void CreateSkill(Skill skill)
        {
            skillRepository.Add(skill);
        }

        public void SaveSkill()
        {
            unitOfWork.Commit();
        }

        #endregion
    }
}
